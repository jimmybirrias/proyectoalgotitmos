package www.uag.mx.main;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author IngMP
 */
public class Triplets implements Comparable{

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + this.p1;
        hash = 79 * hash + this.p2;
        hash = 79 * hash + this.p3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Triplets other = (Triplets) obj;
        if (this.p1 != other.p1) {
            return false;
        }
        if (this.p2 != other.p2) {
            return false;
        }
        if (this.p3 != other.p3) {
            return false;
        }
        return true;
    }
    
    int p1;
    int p2;
    int p3;

    public Triplets(int p1, int p2, int p3) {
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
    }

    public int getP1() {
        return p1;
    }

    public void setP1(int p1) {
        this.p1 = p1;
    }

    public int getP2() {
        return p2;
    }

    public void setP2(int p2) {
        this.p2 = p2;
    }

    public int getP3() {
        return p3;
    }

    public void setP3(int p3) {
        this.p3 = p3;
    }

    @Override
    public String toString() {
        return "Triplets{" + "p1=" + p1 + ", p2=" + p2 + ", p3=" + p3 + '}';
    }

    @Override
    public int compareTo(Object o) {
        Triplets ot = (Triplets)o;
        if (this.p1 == ot.p1) {
           if (this.p2 == ot.p2) {
               if (this.p3 == ot.p3) {
                    return 0;
               }
           }
        }
        return -1;
    }
    
    
}
