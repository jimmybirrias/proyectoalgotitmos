package www.uag.mx.main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author IngMP
 */
public class ArchiveGen {
        
    public static void createFile(String fileName, ArrayList<Integer> fileContent) {
        Writer fileWriter = null;
	    try {
		fileWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(fileName))));
                System.out.println(fileContent.size());
                for (int i = 0; i < fileContent.size(); i++) {
                    try {
                        fileWriter.write(""+fileContent.get(i)+"\n");
                        fileWriter.flush();
                    } catch (IOException ex) {
                        Logger.getLogger(ArchiveGen.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                System.out.println("Finnished the following file: "+fileName);
                
            } catch (FileNotFoundException e) {
                System.out.println("File name: "+fileName+" ---- "+e.getMessage());
	}
           finally {
                if (fileWriter!= null) try {
                    fileWriter.close();
                } catch (IOException ex) {
                    Logger.getLogger(ArchiveGen.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }
            
                /*
        try {
        FileOutputStream file = new FileOutputStream(fileName);
        ObjectOutputStream outputObject = new ObjectOutputStream(file);
            for (int i = 0; i < fileContent.size(); i++) {
               outputObject.writeInt(fileContent.get(i)); 
            }
        outputObject.close(); 
        } catch(IOException ex) {
            System.out.println("Input Output Exception");
        }*/
    }
   
    public static void createFile(String fileName, HashMap<Triplets, Long> fileContent) {
        Writer fileWriter = null;
	    try {
		fileWriter = new BufferedWriter(
				new OutputStreamWriter(
						new FileOutputStream(
                                    new File(fileName))));
                for (int i = 0; i < fileContent.size(); i++) {
                    try {
                        fileWriter.write("La tripleta es: "+((Triplets)(fileContent.keySet().toArray()[i])).toString()+"\n");
                        fileWriter.write("Se repite: "+fileContent.get(fileContent.keySet().toArray()[i])+"\n");
                        fileWriter.flush();
                    } catch (IOException ex) {
                        Logger.getLogger(ArchiveGen.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                
            } catch (FileNotFoundException e) {
		 System.out.println("File name: "+fileName+" ---- "+e.getMessage());
		
	}
           finally {
                if (fileWriter!= null) try {
                    fileWriter.close();
                } catch (IOException ex) {
                    Logger.getLogger(ArchiveGen.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }
    }
 
    public static ArrayList<Integer> readFile(String fileName) {
        ArrayList<Integer> fileContent = new ArrayList<Integer>();
        try{
            String currentLine;
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
           while((currentLine = reader.readLine()) != null){
                fileContent.add(Integer.parseInt(currentLine));
           }
        }
        catch(FileNotFoundException ex){
            System.out.println("Archivo no existe");
        }
        catch(IOException ex){
            System.out.println("Archivo no existe");
        }
        /*
        try {
        FileInputStream file = new FileInputStream(fileName);
        ObjectInputStream inputObject = new ObjectInputStream(file);
            for (int i = 0; i < inputObject.available(); i++) {
                fileContent.add(inputObject.readInt());
            }
        inputObject.close(); 
        } catch(IOException ex) {
            System.out.println("Input Output Exception");
        } */
        return fileContent;
    }

}
