package www.uag.mx.main;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeSet;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author IngMP
 */
public class Processor {
    ArrayList <Integer> fileContent = new ArrayList();
    TreeSet <Integer> numbers = new TreeSet();
    ArrayList <Triplets> triplets= new ArrayList();
    public String rootFolder;

    public String getRootFolder() {
        return rootFolder;
    }

    public void setRootFolder(String rootFolder) {
        this.rootFolder = rootFolder;
    }
    
    public void numGenerator (boolean isHundredFiles) {
        if (isHundredFiles == false) {
            int randomNumber;
            for (int i = 0; i < 1000000; i++) {
                randomNumber=((int)(Math.random()*201)-100);
                fileContent.add(randomNumber);
                /*
                if(fileContent.size()==1000) ArchiveGen.createFile("1Knums.txt", fileContent);
                if(fileContent.size()==2000) ArchiveGen.createFile("2Knums.txt", fileContent);
                if(fileContent.size()==5000) ArchiveGen.createFile("5Knums.txt", fileContent);
                if(fileContent.size()==10000) ArchiveGen.createFile("10Knums.txt", fileContent);
                if(fileContent.size()==100000) ArchiveGen.createFile("100Knums.txt", fileContent);
                if(fileContent.size()==1000000) ArchiveGen.createFile("1Mnums.txt", fileContent);*/
            } 
            ArchiveGen.createFile(rootFolder+"/1Knums.txt", new ArrayList(fileContent.subList(0, 1000)));
            ArchiveGen.createFile(rootFolder+"/2Knums.txt", new ArrayList(fileContent.subList(0, 2000)));
            ArchiveGen.createFile(rootFolder+"/5Knums.txt", new ArrayList(fileContent.subList(0, 5000)));
            ArchiveGen.createFile(rootFolder+"/10Knums.txt", new ArrayList(fileContent.subList(0, 10000)));
            ArchiveGen.createFile(rootFolder+"/100Knums.txt", new ArrayList(fileContent.subList(0, 100000)));
            ArchiveGen.createFile(rootFolder+"/1mnums.txt", new ArrayList(fileContent.subList(0, 1000000)));
        } else {
            int randomNumber;
            String fatherFolder=rootFolder+"/graficas";
            File f=new File(fatherFolder);
            if(!f.exists()){
                 f.mkdirs();
                 for (int i = 0; i < 100; i++) {
                    fileContent = new ArrayList();
                    for (int k = 0; k < 100000; k++) {
                        randomNumber=((int)(Math.random()*201)-100);
                        fileContent.add(randomNumber);
                    } 
                    ArchiveGen.createFile(rootFolder+"/graficas/HundredF"+i+".txt", fileContent);
                }
            }else{
                for (int i = 0; i < 100; i++) {
                    fileContent = new ArrayList();
                    for (int k = 0; k < 100000; k++) {
                        randomNumber=((int)(Math.random()*201)-100);
                        fileContent.add(randomNumber);
                    } 
                    ArchiveGen.createFile(rootFolder+"/graficas/HundredF"+i+".txt", fileContent);
                }
            }
        }
    }
    
    public long fileTriplets(String fileName) {
        System.out.println("Woking with the following file: "+fileName);
        long startTime = System.currentTimeMillis();
        ArrayList <Integer> fileContent = ArchiveGen.readFile(fileName);
        HashMap<Integer , Integer> values = new HashMap <> ();
        
        for(int value : fileContent) {
            if(values.containsKey(value)) {
                int keyValue = (int)values.get(value);
                values.replace(value, keyValue+1);
            } else {
                values.putIfAbsent(value, 1);
            }
        }
        HashMap<Triplets, Long> resultFileContent = new HashMap<>();
        Object A[]=values.keySet().toArray();
        
        for(int i = 0; i < values.keySet().size();i++){
            for(int j = 0; j < A.length;j++){
                for(int k = 0; k < A.length;k++){
                    if(((int)A[i]+(int)A[j]+(int)A[k])==0){
                        Triplets temp = new Triplets((int)A[i],(int)A[j],(int)A[k]);
                        Long repeatedTimes=(long)values.get(A[i])*(int)values.get(A[j])*(int)values.get(A[k]);
                        resultFileContent.put(temp, repeatedTimes);
                    }
                } 
            }
        }
        ArchiveGen.createFile(fileName.substring(0,fileName.length()-4)+"_Results.txt", resultFileContent);
        System.out.println("Finnish with the following file: "+fileName.substring(0,fileName.length()-4));
        long endTime   = System.currentTimeMillis();
        long totalTime = endTime - startTime;
        return totalTime;
    }
    
    public String generateFile(int number){
       int randomNumber;
            String fatherFolder=rootFolder+"/graficas";
            File f=new File(fatherFolder);
            if(!f.exists()){
                f.mkdirs();
                fileContent = new ArrayList();
                for (int k = 0; k < 100000; k++) {
                    randomNumber=((int)(Math.random()*201)-100);
                    fileContent.add(randomNumber);
                }
                ArchiveGen.createFile(rootFolder+"/graficas/HundredF"+number+".txt", fileContent);
                return rootFolder+"/graficas/HundredF"+number+".txt";
            }else{       
                fileContent = new ArrayList();
                for (int k = 0; k < 100000; k++) {
                    randomNumber=((int)(Math.random()*201)-100);
                    fileContent.add(randomNumber);
                } 
                ArchiveGen.createFile(rootFolder+"/graficas/HundredF"+number+".txt", fileContent);
                return rootFolder+"/graficas/HundredF"+number+".txt";
            } 
    }
}
