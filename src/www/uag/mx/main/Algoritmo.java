/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package www.uag.mx.main;

import java.util.Arrays;

/**
 *
 * @author jaime
 */
public class Algoritmo {
    String name;
    double average,maximum,minimum;
    double result[];

    public Algoritmo(String name, double[] result) {
        this.name = name;
        this.result = result;
    }

    public double getAverage() {
        double temp = 0;
        for (int i = 0; i < result.length; i++) {
            temp+=result[i];
        }
        System.out.println("Promedio: "+temp);
        average=(temp/result.length);
        return average;
    }

    public double getMaximum() {
        maximum=Arrays.stream(result).max().getAsDouble();
        return maximum;
    }

    public double getMinimum() {
        minimum=Arrays.stream(result).min().getAsDouble();
        return minimum;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Algoritmo{" + "name=" + name + ", average=" + getAverage() + ", maximum=" + getMaximum() + ", minimum=" + getMinimum() + '}';
    }
    
      
    
}
