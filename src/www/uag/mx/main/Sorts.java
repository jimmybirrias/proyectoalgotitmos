package www.uag.mx.main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author IngMP
 */
public class Sorts {
    static int [] helper;
    List<Integer> l=new ArrayList<>();
    
    public static void callSorts(String fileName){
        //bubbleSort(ArchiveGen.readFile(fileName));
        //insertionSort(ArchiveGen.readFile(fileName));
        //selectionSort(ArchiveGen.readFile(fileName));
        ArrayList <Integer> t = ArchiveGen.readFile(fileName); 
        Integer arr[] = new Integer[t.size()]; 
        arr = t.toArray(new Integer[t.size()]);
        //arr=quickSort(arr,0,arr.length-1);
        //arr=bubbleSort(t);
        System.out.println(Arrays.toString(arr));
    }
    public static double bubbleSort(ArrayList<Integer> t){
        long startTime = System.currentTimeMillis();
        Integer arr[] = new Integer[t.size()]; 
        arr = t.toArray(new Integer[t.size()]);
        boolean swapped = true;
        int j = 0;
        int tmp;
        while (swapped) {
              swapped = false;
              j++;
              for (int i = 0; i < arr.length - j; i++) {                                       
                    if (arr[i] > arr[i + 1]) {                          
                          tmp = arr[i];
                          arr[i] = arr[i + 1];
                          arr[i + 1] = tmp;
                          swapped = true;
                    }
              }                
        }
        long endTime   = System.currentTimeMillis();
        long totalTime = endTime - startTime;
        return (double)((double)totalTime);
    }
    public static double insertionSort(ArrayList<Integer> t){
        long startTime   = System.currentTimeMillis();
        Integer arr[] = new Integer[t.size()]; 
        arr = t.toArray(new Integer[t.size()]);
             int temp;
        for (int i = 1; i < arr.length; i++) {
            for(int j = i ; j > 0 ; j--){
                if(arr[j] < arr[j-1]){
                    temp = arr[j];
                    arr[j] = arr[j-1];
                    arr[j-1] = temp;
                }
            }
        }
        long endTime   = System.currentTimeMillis();
        long totalTime = endTime - startTime;
        return (double)((double)totalTime);
   
    }
    public static double selectionSort(ArrayList<Integer> t){
        long startTime   = System.currentTimeMillis();
        Integer arr[] = new Integer[t.size()]; 
        arr = t.toArray(new Integer[t.size()]);
        for (int i = 0; i < arr.length - 1; i++) {
            int index = i;
            for (int j = i + 1; j < arr.length; j++) if (arr[j] < arr[index]) index = j;
                
            int smallerNumber = arr[index]; 
            arr[index] = arr[i];
            arr[i] = smallerNumber;
        }
        long endTime   = System.currentTimeMillis();
        long totalTime = endTime - startTime;
        return (double)((double)totalTime);
    }
    
    public static Integer [] quickSort(Integer arr[],int low, int high){

        if (arr == null || arr.length == 0) return arr;

        if (low >= high) return arr;    

        int middle = low + (high - low) / 2;
	int pivot = arr[middle];
	int i = low, j = high;
               
	while (i <= j) {
            while (arr[i] < pivot) {
                i++;
            }
            while (arr[j] > pivot) {
                j--;
            }
            if (i <= j) {
                int temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
                i++;
                j--;
            }
	}
	if (low < j)  quickSort(arr, low, j);
 
	if (high > i) quickSort(arr, i, high);
	
        return arr;
    }
    
    public static void mergeSort(int low, int high, Integer[] numbers) {
            if (low < high) {
                int middle = low + (high - low) / 2;
                mergeSort(low, middle, numbers);
                mergeSort(middle + 1, high, numbers);
                merge(low, middle, high, numbers);
            }
        }

    public static void merge(int low, int middle, int high, Integer[] numbers) {
            for (int i = low; i <= high; i++) {
                helper[i] = numbers[i];
            }
            int i = low;
            int j = middle + 1;
            int k = low;
            while (i <= middle && j <= high) {
                if (helper[i] <= helper[j]) {
                    numbers[k] = helper[i];
                    i++;
                } else {
                    numbers[k] = helper[j];
                    j++;
                }
                    k++;
                }
                while (i <= middle) {
                    numbers[k] = helper[i];
                    k++;
                    i++;
                }
        }
    
    public static double[] getTimeResultSelection(String rootFolder, int files){
        if(rootFolder == null){
            System.out.println("Root folder can not be null");
            return null;
        }else{
            double result[]=new double[files];
            for (int i = 0; i < files; i++) {
                System.out.println(rootFolder+"/graficas"+"/HundredF" + i + ".txt");
                result[i]=selectionSort(ArchiveGen.readFile(rootFolder+"/graficas"+"/HundredF" + i + ".txt"));
                System.out.println(result[i]);
            }
            return result;
        }
    }
     
    public static double[] getTimeResultInsertion(String rootFolder, int files){
        if(rootFolder == null){
            System.out.println("Root folder can not be null");
            return null;
        }else{
            double result[]=new double[files];
            for (int i = 0; i < files; i++) {
                System.out.println(rootFolder+"/graficas"+"/HundredF" + i + ".txt");
                result[i]=insertionSort(ArchiveGen.readFile(rootFolder+"/graficas"+"/HundredF" + i + ".txt"));
                System.out.println(result[i]);
            }
            return result;
        }
    }
    
    public static double[] getTimeResultBubble(String rootFolder, int files){
        if(rootFolder == null){
            System.out.println("Root folder can not be null");
            return null;
        } else {
            double result[]=new double[files];
            for (int i = 0; i < files; i++) {
                System.out.println(rootFolder+"/graficas"+"/HundredF" + i + ".txt");
                result[i]=bubbleSort(ArchiveGen.readFile(rootFolder+"/graficas"+"/HundredF" + i + ".txt"));
                System.out.println(result[i]);
            }
            return result;
        }  
    }
    
    public static double[] getTimeResultQuick(String rootFolder, int files){
        if(rootFolder == null){
            System.out.println("Root folder can not be null");
            return null;
        } else{
            double result[]=new double[files];
            for (int i = 0; i < files; i++) {
                System.out.println(rootFolder+"/graficas"+"/HundredF" + i + ".txt");
                ArrayList <Integer> t = ArchiveGen.readFile(rootFolder+"/graficas"+"/HundredF" + i + ".txt"); 
                Integer arr[] = new Integer[t.size()]; 
                arr = t.toArray(new Integer[t.size()]);
                long startTime   = System.currentTimeMillis();
                quickSort(arr, 0,arr.length-1);
                long endTime   = System.currentTimeMillis();
                long totalTime = endTime - startTime;
                result[i]=totalTime;
                System.out.println(result[i]);
            }
            return result;
        }
    }
    
    public static double[] getTimeResultMerge(String rootFolder, int files){
        if(rootFolder == null){
            System.out.println("Root folder can not be null");
            return null;
        } else{
            double result[]=new double[files];
            for (int i = 0; i < files; i++) {
                System.out.println(rootFolder+"/graficas"+"/HundredF" + i + ".txt");
                ArrayList <Integer> t = ArchiveGen.readFile(rootFolder+"/graficas"+"/HundredF" + i + ".txt"); 
                Integer arr[] = new Integer[t.size()]; 
                arr = t.toArray(new Integer[t.size()]);
                helper = new int[arr.length];
                long startTime   = System.currentTimeMillis();
                mergeSort(0, arr.length-1, arr);
                long endTime   = System.currentTimeMillis();
                long totalTime = endTime - startTime;
                result[i]=totalTime;
                System.out.println(result[i]);
            }
            return result;
        }
    }
}
