/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package www.uag.mx.ui;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Toolkit;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import www.uag.mx.main.Algoritmo;
import www.uag.mx.main.ArchiveGen;
import www.uag.mx.main.Sorts;


/**
 *
 * @author jaime
 */
public final class Graficas extends javax.swing.JFrame {
    private JFreeChart chart;
    private final Random rnd;
    public String rootFoler;
    private final int semilla=100000;
    private final int files=100;
    MainWindow father;
    
    /** Creates new form Graficas
     * @param rootFolder
     * @param father */
    public Graficas(String rootFolder, MainWindow father) {
        this.father=father;
        rnd=new Random();
        this.rootFoler=rootFolder;
        initComponents();
        super.setSize(Toolkit.getDefaultToolkit().getScreenSize());
        System.out.println("Size: "+Toolkit.getDefaultToolkit().getScreenSize().toString());
    }

    public Graficas(MainWindow father) {
        this.father=father;
        rnd=new Random();
        initComponents();
        super.setSize(Toolkit.getDefaultToolkit().getScreenSize());
        System.out.println("Size: "+Toolkit.getDefaultToolkit().getScreenSize().toString());
    }
    
    
    
    private ArrayList<Integer> generateContent(){
        ArrayList<Integer> cnum=new ArrayList<>();
        for(int i=0;i<semilla;i++){
                cnum.add(rnd.nextInt(201)-100);
        }
        return cnum;
    }
    
    public void generateFiles(String rootFolder){
        rootFolder+="/graficas";
        File f=new File(rootFolder);
        if(f.exists() && f.isDirectory()){
            if(f.list().length!=files){
               for (int i = 0; i < files; i++) {
                    ArrayList<Integer> cnum=generateContent();
                    ArchiveGen.createFile(rootFolder+"/HundredF"+i+".txt", cnum);
               } 
            }
        }else{
            f.mkdir();
            for (int i = 0; i < files; i++) {
                ArrayList<Integer> cnum=generateContent();
                ArchiveGen.createFile(rootFolder+"/HundredF"+i+".txt", cnum);
            }
        }
    }
   
    public void initChartXY(int type){
        String path=rootFoler;
        generateFiles(path);
        double result[]=null;
        switch (type) {
            case 0:
                result=Sorts.getTimeResultBubble(path,files);
                break;
            case 1:
                result=Sorts.getTimeResultInsertion(path,files);
                break;
            case 2:
                result=Sorts.getTimeResultSelection(path,files);
                break;
            default:
                break;
        }
        
        if(result==null){
            javax.swing.JOptionPane.showMessageDialog(this, "Root folder is null, desde el graficador");
        }else{
            System.out.println(Arrays.toString(result));
            XYSeries[] line=new XYSeries[1];
            XYSeriesCollection dataset = new XYSeriesCollection();
            for (int i = 0; i < line.length; i++) {
                line[i]=new XYSeries("Series "+i);
                for (int j = 0; j < files; j++) {
                    line[i].add(j, result[i]);
                }
                dataset.addSeries(line[i]);
            }
            chart= ChartFactory.createXYLineChart(
             "Tiempos","Category","Score" ,(XYDataset)dataset,PlotOrientation.VERTICAL,true , true , false);
            jPanel1.setLayout(new java.awt.BorderLayout());
            ChartPanel CP = new ChartPanel(chart);
            XYPlot plot = chart.getXYPlot();
            XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
            for (int i = 0; i < 1; i++) {
                renderer.setSeriesPaint(i,new Color(rnd.nextInt(255), rnd.nextInt(255), rnd.nextInt(255)));
            }
            plot.setRenderer(renderer); 
            jPanel1.add(CP,BorderLayout.CENTER);
            jPanel1.validate();
        }
        
    }

    public void initCharAlgoritmos(){
        DefaultCategoryDataset dataset = new DefaultCategoryDataset( );
        for (int i = 0; i < father.algGraficar.size(); i++) {
            Algoritmo temp=father.algGraficar.get(i);
            dataset.addValue(temp.getAverage(), "Promedio" ,(i+1)+"");
            dataset.addValue(temp.getMaximum(), "Maximo" ,(i+1)+"");
            dataset.addValue(temp.getMinimum(), "Minimo" ,(i+1)+"");
        }
        
        chart=ChartFactory.createLineChart("Algoritmos VS","Algoritmo","Tiempo en ms",dataset,PlotOrientation.VERTICAL,true,true,false);
        CategoryPlot plot = (CategoryPlot) chart.getPlot();
        LineAndShapeRenderer renderer = (LineAndShapeRenderer) plot.getRenderer();
        jPanel1.setLayout(new java.awt.BorderLayout());
        renderer.setSeriesPaint( 0 , Color.MAGENTA );
        renderer.setSeriesPaint( 1 , Color.RED );
        renderer.setSeriesPaint( 2 , Color.BLUE );
        renderer.setSeriesStroke( 0 , new BasicStroke( 1.5f ) );
        renderer.setSeriesStroke( 1 , new BasicStroke( 1.5f ) );
        renderer.setSeriesStroke( 2 , new BasicStroke( 1.5f ) );
        ChartPanel CP = new ChartPanel(chart);
        jPanel1.add(CP,BorderLayout.CENTER);
        jPanel1.validate();
        
    }
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 922, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 480, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables

    public void initChartLine(int type, String title, String alg) {
        String path=rootFoler;
        generateFiles(path);
        double result[]=null;
        switch (type) {
            case 0:
                result=Sorts.getTimeResultBubble(path,files);
                break;
            case 1:
                result=Sorts.getTimeResultInsertion(path,files);
                break;
            case 2:
                result=Sorts.getTimeResultSelection(path,files);
                break;
            case 3:
                result=Sorts.getTimeResultQuick(path,files);
                break;
            case 4:
                result=Sorts.getTimeResultMerge(path, files);
                break;
            default:
                break;
        }
        
        if(result==null){
            javax.swing.JOptionPane.showMessageDialog(this, "Root folder is null, desde el graficador");
        } else{
            try {
                System.out.println(Arrays.toString(result));
                Algoritmo a=new Algoritmo(alg, result);
                father.algGraficar.add(a);
                System.out.println(a.toString());
                System.out.println("Tamaño de lista: "+father.algGraficar.size());
                double average=a.getAverage();
                DefaultCategoryDataset dataset = new DefaultCategoryDataset( );
                File filesort = new File(path+"/resultadosSortfiles.txt");
                FileOutputStream is=new FileOutputStream(filesort);
                OutputStreamWriter osw = new OutputStreamWriter(is);
                Writer w = new BufferedWriter(osw);
                for (int i = 0; i < result.length; i++) {
                    dataset.addValue(result[i], "Tiempo" ,(i+1)+"");
                    dataset.addValue(average, "Promedio: "+average, (i+1)+"");
                    System.out.println("Tiempo para el archivo("+i+"): "+result[i]);
                    w.write("Tiempo para el archivo("+i+"): "+result[i]);
                }   
                chart=ChartFactory.createLineChart("Algoritmo: "+title,"Archivos","Tiempo en ms",dataset,PlotOrientation.VERTICAL,true,true,false);
                CategoryPlot plot = (CategoryPlot) chart.getPlot();
                LineAndShapeRenderer renderer = (LineAndShapeRenderer) plot.getRenderer();
                jPanel1.setLayout(new java.awt.BorderLayout());
                renderer.setSeriesPaint( 0 , Color.RED );
                renderer.setSeriesPaint( 1 , Color.GREEN );
                renderer.setSeriesStroke( 0 , new BasicStroke( 1.5f ) );
                renderer.setSeriesStroke( 1 , new BasicStroke( 1.5f ) );
                ChartPanel CP = new ChartPanel(chart);
                jPanel1.add(CP,BorderLayout.CENTER);
                jPanel1.validate();
            } catch (Exception ex) {
                Logger.getLogger(Graficas.class.getName()).log(Level.SEVERE, null, ex);
            } 
        }
    }
    
}
